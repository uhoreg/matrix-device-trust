Device trust demo
=================

This webapp demonstrates trust between devices in Matrix as proposed in
https://github.com/matrix-org/matrix-doc/pull/1756.  It depends on:
- matrix-js-sdk from https://github.com/matrix-org/matrix-js-sdk/pull/832
- synapse from https://github.com/matrix-org/synapse/pull/4970

It currently hard-codes various configuration options, including:
- two users: @alice:example.com and @bob:example.com
- the homeserver location (http://localhost:8008)

Using this may require changing these values.  To use the demo, the two users
must share an encrypted room.

To begin, run `npm start`, and visit http://localhost:3000 in several browser
tabs.  Each tab will be a separate session.  When the page is opened, it will
prompt you to log in, and you can log in as either alice or bob.

The page will then display two columns: one with alice's devices, and one with
bob's devices.  For each device, it will display the device ID (the device
belonging to that session will have a "*" in front), and the device trust
status.  If the device is not verified, then it will have a "Verify" button as
well, which will mark the device as verified and send an attestation to the
server.

The trust status can show a "+" to indicate that the cross-signing key is
verified, "T" to indicate that the cross-signing key is TOFU (Trust on first
use), "V" to indicate that a device key is verified directly, and "N" to
indicate that a key is not verified.

How to use cross-signing in the SDK
-----------------------------------

### Overview

The JS SDK maintains two sets of cross-signing keys:

- keys that are received from the server, and stored in the crypto devicelist.
  These can be retrieved by calling `getStoredCrossSigningForUser`, and will be
  a `CrossSigningInfo` object. (FIXME: would it be better to change it to just
  return the key ID? I think that's all that clients need.)
- the user's own trusted cross-signing keys, stored in the crypto object.  The
  ID for this key can be retrieved by calling `getCrossSigningId`.

If the user wants to set up cross-signing, or replace their cross signing key,
the client can create a new cross-signing key by calling the
`resetCrossSigningKeys` method.  When the cross-signing key is reset, the
private keys will be provided, which the client should save somewhere safe
(such as some secure storage, or letting the user write it down in some
human-readable form).

If cross-signing has been set up on another client, the JS SDK will fetch the
key from the server and emit a `cross-signing.newKey` event to confirm the key.
(This happens when the client fetches the user's device keys from the server.)
(FIXME: the event may fire multiple times until the key is accepted -- this
will probably be fixed in the SDK)  See the verifyXS function in the demo's
index.js file.

The private keys for the cross-signing keys can be handled through the Secrets
module (`src/crypto/Secrets.js`) (see also
https://github.com/matrix-org/matrix-doc/pull/1946).  The module is exposed in
the MatrixClient class as the functions `addSecretKey` (create a key to encrypt
secrets), `storeSecret` (encrypt and store a secret), `getSecret` (retrieve and
decrypt a secret), `isSecretStored` (check if a secret is stored), and
`requestSecret` (request the secret from another client).  There is currently
no way of discovering what keys are available for encrypting/decrypting
secrets.  I'd suggest for now to just use a key with ID "main".

Suggested login flow:

- user logs in
- client caches user's password
- client waits until initial sync completes and client fetches the device keys,
  to see if cross-signing has been set up on a different device
- if not:
  - check if user has backup setup
    - if yes, ask for the backup key, so that the same key can be used to
      encrypt the cross-signing keys
      - create new cross-signing keys, using cached password to send to public
        keys server, and using the backup key to encrypt the private keys for
        storage on the server (note: some people don't want to store the keys
        on the server)
    - if no, ask if the user wants to set up cross signing and key backups
      - if yes, set it up, using the same key for backups as for encrypting the
        private cross-signing keys
- if cross-signing has been set up on a different device
  - ask the user to verify the key, either by fetching the key from another
    device, or by retrieving it from the server (or maybe manually typing it in?)

Cross-signing uses the following events:

- `cross-signing.savePrivateKeys`: called when private keys need to be saved.
  The handler will be called with an object with properties representing the
  keys that need to be saved.  Possible keys are: "master", "self_signing",
  "user_signing".  The values will be `UInt8Array`s.

- `cross-signing.getKey`: called when a private key is needed.  The handler will
  be called with an object with the following properties:
  - `type`: (string) the type of key needed.  Possible values are: "master",
    "self_signing", "user_signing".
  - `done`: (function(UInt8Array)) callback to call with the requested key
  - `cancel`: (function()) callback to call if the key cannot be provided
  - `error`: (string) Optional.  Error string to display to the user.  Normally
    provided if a previously provided key was invalid.

    TODO: I'm not sure if using the `error` field and re-emitting the event is
    a great way to handle incorrect keys.  Another option may be to have the
    `done` function throw an exception if the key doesn't match, but that also
    doesn't seem too appealing.

- `cross-signing.newKey`: called when a new cross-signing key is provided from
  the server.  The handler will be called with an object with the following
  properties:
  - `publicKey`: (string) the public key received from the server
  - `type`: (string) the type of key that was received.  Currently will only be
    "master".
  - `error`: (string) Optional.  Error string to display to the user.  Normally
    provided if a previously provided key was invalid.
  - `done`: (function(UInt8Array)) callback to call with the private key
    corresponding to the given public key.
  - `cancel`: (function()) callback to call if the private key cannot be
    provided, indicating that the client does not accept the cross-signing key.

  The handler must verify the key by providing the private key for the provided
  public key, for example by fetching the key from the server, or from another
  client. When called, the handler may wish to obtain the private keys for all
  the other cross-signing keys as well.

- `crypto.secrets.request`: called when a secret has been requested by another
  client.  The handler will be called with an object with the following
  properties:
  - `name`: (string) The name of the secret being requested.  For cross-signing,
    the possible names are: "m.cross-signing.master",
    "m.cross-signing.user_signing", "m.cross-signing.self_signing", which
    request the private keys for the master, user-signing, and self-signing keys.
  - `user_id`: (string) The user ID of the client requesting the secret.  Must
    be the same as the client's user for cross-signing.
  - `device_id`: (string) The device ID of the client requesting the secret.
  - `request_id`: (string) The ID of the request.  Used to match a
    corresponding `crypto.secrets.request_cancelled`.  The request ID will be
    unique per sender, device pair.
  - `device_trust`: (int) The trust status of the device requesting the secret.
    Will be a bit mask in the same form as returned by
    MatrixClient#checkDeviceTrust.
  - `send`: (function(string)) Callback to call with the secret.  Private keys
    should be sent as a base64-encoded string.

  Clients should ensure that the requesting device is allowed to have the
  secret.  For example, if the device is not already trusted, a verification
  should be performed before sharing the secret.  The client may also wish to
  prompt the user before sharing the secret.

- `crypto.secrets.request_cancelled`: called when a secret request has been
  cancelled.  If the client is prompting the user to ask whether they want to
  share a secret, the prompt can be dismissed.  The handler will be called with
  an object with the following properties:
  - `user_id`: (string) The user ID of the client that had requested the secret.
  - `device_id`: (string) The device ID of the client that had requested the
    secret.
  - `request_id`: (string) The ID of the original request.

Bonus: for sending verification requests to users (verifying without needing to
select a device):

Call client.requestVerification with the user ID in order to make a request,
and modify the crypto.verification.start handler to not prompt the user if it
was sent in response to a request.

In order to respond to a verification request, handle the
"crypto.verification.request" event.
