import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ReactModal from 'react-modal';
import './index.css';
import * as serviceWorker from './serviceWorker';
import sdk from 'matrix-js-sdk';
import {verificationMethods} from 'matrix-js-sdk/lib/crypto';
import {keyForExistingBackup, keyForNewBackup} from 'matrix-js-sdk/lib/crypto/backup_password';

global.Olm.init();

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            client: null,
        };
        this.username = React.createRef();
        this.password = React.createRef();
        this.callbacks = {
            setupXS: this.setupXS.bind(this),
        }
    }
    render() {
        if (this.state.client) {
            return (
                    <div className="App">
                      <h1>Logged in as {this.state.client.getUserId()}</h1>
                      <button type="button" onClick={() => this.logout()}>Log out</button>
                      <div className="deviceContainer">
                        <DeviceList userid="@alice:example.com" client={this.state.client} callbacks={this.callbacks}/>
                        <DeviceList userid="@bob:example.com" client={this.state.client} callbacks={this.callbacks}/>
                      </div>
                      <ReactModal isOpen={this.state.showPasswordModal} contentLabel="Cross signing">
                        <GetPassword message={this.state.passwordMessage} onDone={this.state.passwordResolve}></GetPassword>
                      </ReactModal>
                    </div>
            );
        } else {
            return (
                <div className="App">
                    <form onSubmit={(e) => this.login(e)}>
                      <div>
                        <label htmlFor="username">Username: </label>
                        <input type="text" name="username" id="username" ref={this.username} />
                      </div>
                      <div>
                        <label htmlFor="password">Password: </label>
                        <input type="password" name="password" id="password" ref={this.password} />
                      </div>
                      <div>
                        <input type="submit" value="Log in" />
                      </div>
                    </form>
                </div>
            );
        }
    }

    async login(e) {
        e.preventDefault();
        const username = this.username.current.value;
        const password = this.password.current.value;
        let client = sdk.createClient("http://localhost:8008");
        // FIXME: handle login failure
        const response = await client.login("m.login.password", {
            user: username,
            password: password
        });
        client = sdk.createClient({
            baseUrl: "http://localhost:8008",
            accessToken: response.access_token,
            userId: response.user_id,
            deviceId: response.device_id,
            sessionStore: new sdk.WebStorageSessionStore(window.sessionStorage),
            cryptoStore: new sdk.MemoryCryptoStore(),
            useAuthorizationHeader: true,
            verificationMethods: [verificationMethods.SAS],
        });
        await client.initCrypto();
        // Using internal members.  Naughty, naughty.
        // forcibly track device lists for the users.
        client._crypto._deviceList.stopTrackingAllDeviceLists = () => {};
        client._crypto._deviceList.startTrackingDeviceList("@bob:example.com");
        client._crypto._deviceList.startTrackingDeviceList("@alice:example.com");
        client.startClient();
        const state = Object.assign({}, this.state);
        state.client = client;
        this.setState(state);

        client.on("crypto.verification.start", async (verifier) => {
            verifier.on("show_sas", (e) => {
                if (window.confirm(e.sas.emoji.map((x) => x[0]).join(""))) {
                    e.confirm();
                } else {
                    e.mismatch();
                }
            });
            await verifier.verify();
        });

        client.on("cross-signing.savePrivateKeys", (e) => {
            const state = Object.assign({}, this.state);
            state.privKeys = Object.assign({}, this.state.privKeys || {}, e);
            this.setState(state);
        });

        client.on("cross-signing.getKey", (e) => {
            if (this.state.privKeys[e.type]) {
                return e.done(this.state.privKeys[e.type]);
            } else {
                e.cancel();
            }
        });

        client.on("crypto.secrets.request", async (e) => {
            const device = await client.getStoredDevice(client.getUserId(), e.device_id);
            if (e.name.startsWith("m.cross_signing.")) {
                if (!device.isVerified()) {
                    if (!this.verifying) {
                        if (!window.confirm(
                            "Do you want to share cross-signing keys with " + e.user_id
                                + " (" + e.device_id + ")?",
                        )) {
                            return;
                        }
                        this.verifying = new Promise(async (resolve, reject) => {
                            const verifier = client.beginKeyVerification(
                                verificationMethods.SAS, e.user_id, e.device_id,
                            );
                            verifier.on("show_sas", (e) => {
                                if (window.confirm(e.sas.emoji.map((x) => x[0]).join(""))) {
                                    e.confirm();
                                } else {
                                    e.mismatch();
                                }
                            });
                            await verifier.verify();
                            delete this.verifying;
                            resolve();
                        });
                    }
                    await this.verifying;
                }
                e.send(
                    Buffer.from(this.state.privKeys[e.name.substring(16)]).toString('base64'),
                );
            }
        });

        client.on("cross-signing.newKey", this.verifyXS.bind(this));

        client.on("crypto.verification.request", async (req) => {
            if (req.methods.includes(verificationMethods.SAS)
                && window.confirm("Do you want to verify keys with " + req.event.getSender()) + "?") {
                const verifier = req.beginKeyVerification(
                    verificationMethods.SAS
                );
                verifier.on("show_sas", (e) => {
                    if (window.confirm(e.sas.emoji.map((x) => x[0]).join(""))) {
                        e.confirm();
                    } else {
                        e.mismatch();
                    }
                });
                await verifier.verify();
            }
        });
    }

    async logout() {
        const client = this.state.client;
        client.stopClient();
        client.clearStores();
        await client.logout();
        const state = Object.assign({}, this.state);
        state.client = null;
        state.devices = {};
        this.setState(state);
    }

    async askPassword(message) {
        const password = await new Promise((resolve, reject) => {
            const state = Object.assign({}, this.state);
            state.passwordMessage = message;
            state.passwordResolve = resolve;
            state.showPasswordModal = true;
            this.setState(state);
        });
        {
            const state = Object.assign({}, this.state);
            state.showPasswordModal = false;
            this.setState(state);
        }
        return password;
    }

    async verifyXS(e) {
        const client = this.state.client;
        if (window.confirm("Cross signing key found. Press OK to retrieve key from the server, or Cancel to request key from another device")) {
            // fetch the private keys from the server
            const password = await this.askPassword("Please enter the password to unlock your cross-signing key");
            const keyData = client.getAccountData("m.secret_storage.key.main").getContent();
            // FIXME: convert the backups to use Secrets module, and redo
            // keyForExistingBackup so that we don't need to make a fake
            // auth_data object
            const key = await keyForExistingBackup({
                auth_data: {
                    private_key_salt: keyData.passphrase.salt,
                    private_key_iterations: keyData.passphrase.iterations,
                }
            }, password);
            client.on("crypto.secrets.getKey", (e) => {
                if (e.keys.main) {
                    e.done("main", key);
                } else {
                    e.cancel(new Error("Unknown key"));
                }
            });
            const privKeys = {
                master: Buffer.from(
                    await client.getSecret("m.cross_signing.master"), 'base64',
                ),
                self_signing: Buffer.from(
                    await client.getSecret("m.cross_signing.self_signing"), 'base64',
                ),
                user_signing: Buffer.from(
                    await client.getSecret("m.cross_signing.user_signing"), 'base64',
                ),
            }
            const state = Object.assign({}, this.state);
            state.privKeys = privKeys;
            this.setState(state);

            e.done(privKeys.master);
        } else {
            // request the private keys from another client
            const privKeys = {
                master: Buffer.from(
                    await client.requestSecret("m.cross_signing.master").promise,
                    "base64",
                ),
                self_signing: Buffer.from(
                    await client.requestSecret("m.cross_signing.self_signing").promise,
                    'base64',
                ),
                user_signing: Buffer.from(
                    await client.requestSecret("m.cross_signing.user_signing").promise,
                    'base64',
                ),
            }
            const state = Object.assign({}, this.state);
            state.privKeys = privKeys;
            this.setState(state);

            e.done(privKeys.master);
        }
    }

    async setupXS() {
        const acctPassword = await this.askPassword("Please enter your account password");
        const client = this.state.client;
        await client.resetCrossSigningKeys({
            type: "m.login.password",
            user: client.getUserId(),
            password: acctPassword,
        });
        const password = await this.askPassword("Please enter a password to encrypt your cross-signing key");
        const key = await keyForNewBackup(password);
        // TODO: use client.addSecretKey instead of poking accountdata directly
        const decryption = new global.Olm.PkDecryption();
        const keyData = {
            algorithm: "m.secret_storage.v1.curve25519-aes-sha2",
            passphrase: {
                algorithm: "m.pbkdf2",
                iterations: key.iterations,
                salt: key.salt,
            },
            pubkey: decryption.init_with_private_key(key.key),
        }

        await client.setAccountData(
            "m.secret_storage.key.main", keyData,
        );

        // wait until the key information comes back through the sync, or else
        // store won't know how to encrypt it
        await new Promise((resolve, reject) => {
            const check = () => {
                if (client.getAccountData("m.secret_storage.key.main")) {
                    resolve();
                } else {
                    window.setTimeout(check, 1000);
                }
            };
            check();
        });

        await client.storeSecret(
            "m.cross_signing.master",
            Buffer.from(this.state.privKeys.master).toString('base64'),
            ["main"],
        );
        await client.storeSecret(
            "m.cross_signing.self_signing",
            Buffer.from(this.state.privKeys.self_signing).toString('base64'),
            ["main"],
        );
        await client.storeSecret(
            "m.cross_signing.user_signing",
            Buffer.from(this.state.privKeys.user_signing).toString('base64'),
            ["main"],
        );
    }
}

class DeviceList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            devices: [],
            trusted: {},
            userTrust: 0,
        };
        this.onDeviceDownloaded = this.onDeviceDownloaded.bind(this);
        this.onDeviceVerificationChanged = this.onDeviceVerificationChanged.bind(this);
    }
    componentWillMount() {
        const client = this.props.client;
        client.on("crypto.devicesUpdated", this.onDeviceDownloaded);
        client.on("deviceVerificationChanged", this.onDeviceVerificationChanged);
        client.on("cross-signing.keysChanged", () => {
            this.onDeviceVerificationChanged(client.getUserId());
        });
        this.getDevices();
    }
    async getDevices() {
        const client = this.props.client;

        const state = Object.assign({}, this.state);
        state.userTrust = client.checkUserTrust(this.props.userid)
        state.trusted = {};
        // FIXME: race condition! the state may get set incorrectly if we have
        // multiple requests in flight
        state.devices = await client.getStoredDevicesForUser(this.props.userid);
        state.devices.forEach((dev) => {
            state.trusted[dev.deviceId] = this.props.client.checkDeviceTrust(this.props.userid, dev.deviceId);
        });
        state.crossSigningId = client.getCrossSigningId();
        const crossSigningInfo = client.getStoredCrossSigningForUser(this.props.userid);
        state.serverCrossSigningId = crossSigningInfo && crossSigningInfo.getId();
        this.setState(state);
    }
    render () {
        const state = this.state;
        const client = this.props.client;
        const devices = state.devices.map((dev) => {
            const thisDevice = dev.deviceId === client.deviceId ? "* " : "";
            const verified = thisDevice || dev.isVerified() ?
                  "" :
                  (<button onClick={() => this.verify(dev)}>Verify</button>);
            const trusted = state.trusted[dev.deviceId];
            const trustString = trusted & 7 ?
                  ((trusted & 4 ? "➕" : "")
                   + (trusted & 2 ? "T" : "")
                   + (trusted & 1 ? "V" : ""))
                  : "N";
            return (
                <li key={dev.deviceId}>
                    {thisDevice}{dev.deviceId} ({trustString}) {verified}
                </li>
            )
        });
        const trustString = state.userTrust & 6 ?
              (state.userTrust & 4 ? "➕" : "")
              + (state.userTrust & 2 ? "T" : "")
              : "N";
        const verify = (client.getUserId() === this.props.userid || state.userTrust & 4)
              ? ""
              : (
                      <button onClick={() => this.verifyUser()}>Verify</button>
              );
        let crossSigningStatus = "";
        if (client.getUserId() === this.props.userid) {
            if (state.crossSigningId) {
                if (state.crossSigningId === state.serverCrossSigningId) {
                    crossSigningStatus = (<div>Cross-signing ID: {state.crossSigningId}</div>);
                } else {
                    crossSigningStatus = (
                            <div>
                                Our cross-signing ID: {state.crossSigningId},
                                Server cross-signing ID: {state.serverCrossSigningId}
                            </div>
                    );
                }
            } else {
                if (state.serverCrossSigningId) {
                    crossSigningStatus = (
                            <div>
                                Server cross-signing ID: {state.serverCrossSigningId}
                            </div>
                    );
                } else {
                    crossSigningStatus = (
                            <div>
                                <button type="button" onClick={this.props.callbacks.setupXS}>Set up cross-signing</button>
                            </div>
                    );
                }
            }
        } else if (state.serverCrossSigningId) {
            crossSigningStatus = (
                    <div>Cross-signing ID: {state.serverCrossSigningId}</div>
            );
        }
        return (
            <div className="DeviceList">
                <h1>{this.props.userid} ({trustString}) {verify}</h1>
                {crossSigningStatus}
                <ul>{devices}</ul>
            </div>
        );
    }
    onDeviceDownloaded(users) {
        if (users.includes(this.props.userid) || users.includes(this.props.client.getUserId())) {
            this.getDevices();
        }
    }
    onDeviceVerificationChanged(userId, device) {
        if (userId === this.props.userid || userId === this.props.client.getUserId()) {
            this.getDevices();
        }
    }
    async verify(device) {
        const verifier = this.props.client.beginKeyVerification(
            verificationMethods.SAS, this.props.userid, device.deviceId,
        );
        verifier.on("show_sas", (e) => {
            if (window.confirm(e.sas.emoji.map((x) => x[0]).join(""))) {
                e.confirm();
            } else {
                e.mismatch();
            }
        });
        await verifier.verify();
        await this.getDevices();
    }
    async verifyUser() {
        const verifier = await this.props.client.requestVerification(
            this.props.userid,
            [verificationMethods.SAS],
        );
        // normally with a verifier, you would need to do a
        // verifier.on("show_sas" ...) and await verifier.verify().  However,
        // this is done by the crypto.verification.start event handler, so we
        // don't need to do it here.
    }
}

class GetPassword extends Component {
    constructor(props) {
        super(props);
        this.passphrase = React.createRef();
    }
    componentWillMount() {
        if (this.passphrase.current) {
            this.passphrase.current.value = "";
        }
    }
    render () {
        return (
                <div>
                  <div>
                    {this.props.message}
                  </div>
                  <div>
                    <label htmlFor="passphrase">Enter a passphrase:</label>
                    <input type="password" name="passphrase" id="passphrase" ref={this.passphrase} />
                  </div>
                  <div>
                    <button type="button" onClick={() => this.done()}>OK</button>
                  </div>
                </div>
        );
    }
    done() {
        this.props.onDone(this.passphrase.current.value);
    }
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
